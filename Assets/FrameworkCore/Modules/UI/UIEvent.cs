﻿/*
 *  date: 2018-07-15
 *  author: John-chen
 *  cn: UI操作相关事件名
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// UI相关事件名
    /// </summary>
    public sealed class UIEvent
    {
        /// <summary>
        /// date: 2018-07-15
        /// author: John-Chen
        /// UI加载完成的事件
        /// </summary>
        public static string WindowLoaded { get { return "WindowLoaded";} }

    }
}