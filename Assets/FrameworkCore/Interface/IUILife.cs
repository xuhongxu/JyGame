﻿/*
 *  date: 2018-07-05
 *  author: John-chen
 *  cn: UI窗口生命周期
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// UI窗口生命周期
    /// </summary>
    public interface IUILife
    {
        /// <summary>
        /// 本窗口的对象
        /// </summary>
        GameObject gameObject { get; }

        /// <summary>
        /// 窗口初始化调用
        /// </summary>
        void OnInit();

        /// <summary>
        /// 窗口每次显示前调用
        /// </summary>
        void OnBeforeShow();

        /// <summary>
        /// 窗口每次显示后调用
        /// </summary>
        void OnShow();

        /// <summary>
        /// 窗口每帧更新
        /// </summary>
        /// <param name="deltaTime"></param>
        void OnUpdate(float deltaTime);

        /// <summary>
        /// 窗口影藏时调用
        /// </summary>
        void OnHide();

        /// <summary>
        /// 窗口销毁时调用
        /// </summary>
        void OnRemove();
    }
}