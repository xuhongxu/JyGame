﻿/*
 *  date: 2018-07-20
 *  author: John-chen
 *  cn: 游戏开始入口
 *  en: todo:
 */

using JyFramework;
using UnityEngine;

namespace GameCore.BaseCore
{
    /// <summary>
    /// 游戏开始入口
    /// </summary>
    public class GameApp : SingletonMgr<GameApp>
    {
        /// <summary>
        /// 核心类
        /// </summary>
        public static JyApp App { get { return _jyApp;} }

        /// <summary>
        /// 启动类
        /// </summary>
        public static GameLaunch Launch { get { return _launch; } }

        /// <summary>
        /// 对象
        /// </summary>
        public static GameObject gameObject { get { return _obj;} }

        /// <summary>
        /// 对象
        /// </summary>
        public static Transform transform { get { return _tfm; } }

        /// <summary>
        /// 测试开始节点
        /// </summary>
        public static TestRoot TestRoot { get { return _testRoot;} }

        /// <summary>
        /// 读取启动对象
        /// </summary>
        /// <param name="obj"></param>
        public static void InitSelf(GameObject obj)
        {
            _obj = obj;
            _obj.name = _objName;
            _tfm = obj.transform;

            _launch = obj.GetComponent<GameLaunch>();
            _jyApp = _launch.App;

            GameApp.CreateSingleton();
        }

        /// <summary>
        /// 移除
        /// </summary>
        public override void Remove()
        {
            ResMgr.Ins.Remove();
            LogMgr.Ins.Remove();
            EventMgr.Ins.Remove();
            UIMgr.Ins.Remove();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            AddUseMgrs();
            AddTestScript();
        }

        /// <summary>
        /// 添加所有使用的管理
        /// </summary>
        private void AddUseMgrs()
        {
            // 添加日志类
            LogMgr.CreateSingleton();

            // 添加事件管理器
            EventMgr.CreateSingleton();

            // 添加资源管理器
            ResMgr.CreateSingleton();

            // 添加UI管理器
            UIMgr.CreateSingleton();
        }

        /// <summary>
        /// 添加测试脚本
        /// </summary>
        private void AddTestScript()
        {
            _testObj = new GameObject("TestRoot");
            _testObj.transform.parent = _tfm;
            _testRoot = _testObj.AddComponent<TestRoot>();
        }

        private static JyApp _jyApp;
        private static GameObject _obj;
        private static Transform _tfm;
        private static GameLaunch _launch;
        private static string _objName = "GameApp";

        private static GameObject _testObj;
        private static TestRoot _testRoot;
    }
}